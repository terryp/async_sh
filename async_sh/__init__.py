from __future__ import annotations
import signal
import re

from asyncio import create_subprocess_exec, create_task, IncompleteReadError
from functools import wraps
from locale import getpreferredencoding
from os import EX_OK
from subprocess import PIPE, DEVNULL
from textwrap import dedent
from types import ModuleType
from sys import modules
from itertools import chain

from anyio import create_task_group
from public import public


DEFAULT_ENCODING = getpreferredencoding() or 'UTF-8'


class ErrorReturnCodeMeta(type):
    """ a metaclass which provides the ability for an ErrorReturnCode (or
    derived) instance, imported from one sh module, to be considered the
    subclass of ErrorReturnCode from another module.  this is mostly necessary
    in the tests, where we do assertRaises, but the ErrorReturnCode that the
    program we're testing throws may not be the same class that we pass to
    assertRaises
    """

    def __subclasscheck__(self, o):
        other_bases = {b.__name__ for b in o.__bases__}
        return self.__name__ in other_bases or o.__name__ == self.__name__


@public
class ErrorReturnCode(Exception, metaclass=ErrorReturnCodeMeta):
    """ base class for all exceptions as a result of a command's exit status
    being deemed an error.  this base class is dynamically subclassed into
    derived classes with the format: ErrorReturnCode_NNN where NNN is the exit
    code number.  the reason for this is it reduces boiler plate code when
    testing error return codes:
        try:
            some_cmd()
        except ErrorReturnCode_12:
            print("couldn't do X")
    vs:
        try:
            some_cmd()
        except ErrorReturnCode as e:
            if e.exit_code == 12:
                print("couldn't do X")
    it's not much of a savings, but i believe it makes the code easier to read
    """

    truncate_cap = 750

    def __reduce__(self):
        return self.__class__, (
            self.full_cmd,
            self.stdout,
            self.stderr,
            self.truncate,
        )

    def __init__(self, full_cmd, stdout=None, stderr=None, truncate=True):
        self.full_cmd = full_cmd
        self.stdout = stdout
        self.stderr = stderr
        self.truncate = truncate

        if self.stdout is not None:
            exc_stdout = self.stdout.decode(DEFAULT_ENCODING, 'replace')
            if truncate:
                exc_stdout = exc_stdout[:self.truncate_cap]
                out_delta = len(self.stdout) - len(exc_stdout)
                if out_delta:
                    delta_msg = f'... ({out_delta} more, please see e.stdout)'
                    exc_stdout += delta_msg

            exc_stdout += f'STDOUT:\n{exc_stdout}'
        else:
            exc_stdout = 'STDOUT was not saved.'

        if self.stderr is not None:
            exc_stderr = self.stderr.decode(DEFAULT_ENCODING, 'replace')
            if truncate:
                exc_stderr = exc_stderr[:self.truncate_cap]
                err_delta = len(self.stderr) - len(exc_stderr)
                if err_delta:
                    delta_msg = f'... ({err_delta} more, please see e.stderr)'
                    exc_stdout += delta_msg

            exc_stderr += f'STDERR:\n{exc_stderr}'
        else:
            exc_stderr = 'STDERR was not saved.'

        msg = dedent(f"""


          RAN: {self.full_cmd}

          {exc_stdout}
          {exc_stderr}
        """)

        super().__init__(msg)


@public
class SignalException(ErrorReturnCode):
    pass


@public
class TimeoutException(Exception):
    """ the exception thrown when a command is killed because a specified
    timeout (via _timeout or .wait(timeout)) was hit """

    def __init__(self, exit_code, full_cmd):
        self.exit_code = exit_code
        self.full_cmd = full_cmd
        super(Exception, self).__init__()


SIGNALS_THAT_SHOULD_THROW_EXCEPTION = {
    signal.SIGABRT,
    signal.SIGBUS,
    signal.SIGFPE,
    signal.SIGILL,
    signal.SIGINT,
    signal.SIGKILL,
    signal.SIGPIPE,
    signal.SIGQUIT,
    signal.SIGSEGV,
    signal.SIGSYS,
    signal.SIGTERM,
}


# we subclass AttributeError because:
# https://github.com/ipython/ipython/issues/2577
# https://github.com/amoffat/sh/issues/97#issuecomment-10610629
@public
class CommandNotFound(AttributeError):
    pass


rc_exc_regex = re.compile(
    r"(ErrorReturnCode|SignalException)_((\d+)|SIG[a-zA-Z]+)"
)
rc_exc_cache = {}

SIGNAL_MAPPING = {
    v: k
    for k, v in signal.__dict__.items()
    if re.match(r"SIG[a-zA-Z]+", k)
}


def get_exc_from_name(name) -> ErrorReturnCode:
    """ takes an exception name, like:
        ErrorReturnCode_1
        SignalException_9
        SignalException_SIGHUP
    and returns the corresponding exception.  this is primarily used for
    importing exceptions from sh into user code, for instance, to capture those
    exceptions """

    exc = None
    try:
        return rc_exc_cache[name]
    except KeyError:
        m = rc_exc_regex.match(name)
        if m:
            base = m.group(1)
            rc_or_sig_name = m.group(2)

            if base == "SignalException":
                try:
                    rc = -int(rc_or_sig_name)
                except ValueError:
                    rc = -getattr(signal, rc_or_sig_name)
            else:
                rc = int(rc_or_sig_name)

            exc = get_rc_exc(rc)
    return exc


def get_rc_exc(rc) -> ErrorReturnCode:
    """ takes a exit code or negative signal number and produces an exception
    that corresponds to that return code.  positive return codes yield
    ErrorReturnCode exception, negative return codes yield SignalException
    we also cache the generated exception so that only one signal of that type
    exists, preserving identity """

    try:
        return rc_exc_cache[rc]
    except KeyError:
        pass

    if rc >= 0:
        name = "ErrorReturnCode_%d" % rc
        base = ErrorReturnCode
    else:
        signame = SIGNAL_MAPPING[abs(rc)]
        name = "SignalException_" + signame
        base = SignalException

    exc = ErrorReturnCodeMeta(name, (base,), dict(exit_code=rc))
    rc_exc_cache[rc] = exc
    return exc


def as_iter(coro):
    @wraps(coro)
    def wrapper(*args, **kwargs):
        return (yield from coro(*args, **kwargs).__await__())

    return wrapper


class CommandMeta(type):
    def __getattr__(cls, command):
        return cls(command)

    def __getitem__(cls, command):
        return cls(command)


@public
class Command(metaclass=CommandMeta):
    def __init__(self, command, *args, _env=None, **kwargs):
        self.__command = command
        self.__args = self.__prep_args(args, kwargs)
        self.__env = _env

    def __process_kwargs(self, kwargs):
        for name, value in kwargs.items():
            name_dashed = name.replace('_', '-')
            if len(name) == 1:
                if value:
                    yield f'-{name}'
                    if not isinstance(value, bool):
                        yield str(value)

            elif isinstance(value, bool):
                if value:
                    yield f'--{name_dashed}'
                else:
                    yield f'--no-{name_dashed}'
            else:
                yield f'--{name_dashed}={value}'

    def __prep_args(self, args, kwargs):
        flags = self.__process_kwargs(kwargs)

        str_args = (str(arg) for arg in args)

        return tuple(chain(str_args, flags))

    @as_iter
    async def __await__(self):
        running_command = RunningCommand(
            self.__command,
            self.__args,
            env=self.__env,
        )

        return await running_command

    def iter(self, sep='\n'):
        return RunningCommand(
            self.__command,
            self.__args,
            env=self.__env,
            iter_sep=sep,
        )

    def __aiter__(self):
        return RunningCommand(
            self.__command,
            self.__args,
            env=self.__env,
        )

    def __getitem__(self, name):
        return self(name)

    def __getattr__(self, name):
        return self(name)

    async def start(self, *args, **kwargs) -> RunningCommand:
        cmd = self(*args, **kwargs)
        return await cmd.__start()

    async def __start(self):
        running_command = RunningCommand(
            self.__command,
            self.__args,
            env=self.__env,
        )

        return await running_command.start()

    def __call__(self, *args, **kwargs):
        cls = self.__class__
        return cls(
            self.__command,
            *self.__args,
            *args,
            **kwargs,
        )


@public
class RunningCommand:
    def __init__(
        self,
        command,
        args,
        iter_sep='\n',
        env=None,
    ):
        self.__command = command
        self.__args = args
        self.__env = env
        self.__iter_sep = iter_sep.encode(DEFAULT_ENCODING, 'replace')

        self.__proc = None
        self.__stderr_drain = None
        self.stderr = None
        self.stdout = None
        self.__iter_done = False

    async def __find_cmd_path(self):
        which_call = await create_subprocess_exec(
            '/usr/bin/which',
            self.__command,
            stdout=PIPE,
            stderr=DEVNULL,
            env=self.__env,
        )

        (stdout_data, _) = await which_call.communicate()

        if which_call.returncode != EX_OK:
            if '_' in self.__command:
                cmd = self.__command.replace('_', '-')
                which_call = await create_subprocess_exec(
                    '/usr/bin/which',
                    cmd,
                    stdout=PIPE,
                    stderr=DEVNULL,
                    env=self.__env,
                )

                (stdout_data, _) = await which_call.communicate()

                if which_call.returncode != EX_OK:
                    raise CommandNotFound(self.__command)
            else:
                raise CommandNotFound(self.__command)

        return stdout_data.strip().decode()

    async def start(self, stdout=PIPE, stderr=PIPE):
        cmd = await self.__find_cmd_path()
        self.__cmd_path = cmd
        self.__proc = await create_subprocess_exec(
            cmd,
            *self.__args,
            stderr=stderr,
            stdout=stdout,
            env=self.__env,
        )

        self.stdout = self.__proc.stdout
        self.stderr = self.__proc.stderr

        return self

    @as_iter
    async def __await__(self):
        await self.start()
        (stdout, stderr) = await self.__proc.communicate()

        if self.__proc.returncode != EX_OK:
            full_cmd = f'{self.__cmd_path} ' + ' '.join(self.__args)
            Err = get_rc_exc(self.__proc.returncode)
            raise Err(full_cmd, stdout, stderr)

        return stdout, stderr

    def __aiter__(self):
        return self

    async def ___finish_stderr_drain(self):
        if self.__proc.stderr:
            if self.__stderr_drain is None:
                self.__stderr_drain = create_task(
                    self.__drain_stream(self.__proc.stderr),
                )

            await self.__stderr_drain

    async def __anext__(self):
        if self.__iter_done:
            raise StopAsyncIteration()

        if self.__proc is None:
            await self.start(stderr=DEVNULL)

        try:
            next_value = await self.__proc.stdout.readuntil(self.__iter_sep)
            return next_value
        except IncompleteReadError as err:
            self.__iter_done = True
            await self.wait()
            if err.partial:
                return err.partial

            raise StopAsyncIteration()

    async def __aenter__(self):
        if self.__proc is None:
            await self.start()

        return self

    async def wait(self):
        async with create_task_group() as tg:
            tg.start_soon(self.__proc.wait)
            tg.start_soon(self.__drain_stream, self.__proc.stdout)
            tg.start_soon(self.___finish_stderr_drain)

        if self.__proc.returncode != EX_OK:
            Err = get_rc_exc(self.__proc.returncode)
            full_cmd = f'{self.__cmd_path} ' + ' '.join(self.__args)
            raise Err(full_cmd)

    async def __drain_stream(self, stream):
        while not stream.at_eof():
            # read 1Kb at a time and immediately discard it
            await stream.read(1000)

    __aexit__ = wait


whitelist = set(__all__)  # noqa: F821

__global_scope__ = globals()


class CommandModule(ModuleType):
    def __getattribute__(self, key):
        if key in whitelist:
            return __global_scope__[key]

        exc = get_exc_from_name(key)
        if exc:
            return exc

        if key.startswith('__') and key.endswith('__'):
            return super().__getattribute__(key)

        return Command[key]

    def __getitem__(self, key):
        return Command[key]

    def __call__(self, *args, **kwargs):
        return Command(*args, **kwargs)


modules[__name__].__class__ = CommandModule
